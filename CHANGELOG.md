
# Changelog for workspace

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v6.30.2] - 2024-07-29

- Included upload of files/archives with `fileSize` parameter [#27898]
- Moved to `maven-portal-bom` 4.0.0{-SNAPSHOT} [#28026]

## [v6.30.1] - 2024-06-28

- Includes the `metadata-profile-form-builder-widget` enhancement [#27488]
- Updated `workspace-tree-widget` dependency lower range [#27747#note-20]

## [v6.30.0] - 2024-02-20

- Moved to maven parent 1.2.0
- Equipped with the new Catalogue Publishing Widget [#26825]
- Fixed Incident [#26678]

## [v6.29.0] - 2023-04-04

- Just to release the patch #24929 in the 'storagehub-client-wrapper'

## [v6.28.6] - 2022-09-05

- Just to release the fixes #23676, #23789 implemented in the ws-tree

## [v6.28.5] - 2022-06-27

- [#23523] Updated to maven-portal-bom 3.6.4
- [#23491] Just to release the Publishing Widget enhancement #23491

## [v6.28.3] - 2022-05-02

- [#23225] Just including the enhancement #23225

## [v6.28.2] - 2022-03-24

#### Enhancements

- [#23020] Just including the enhancement #23020

## [v6.28.1] - 2021-12-20

#### Fixes

- [#22578] Including workspace-tree-widget bug fixing

## [v6.28.0] - 2021-11-05

#### Enhancements

- [#22251] Make workspace file size field smart

## [v6.27.1] - 2021-06-22

[#21575] Including workspace-tree-widget bug fixing
Moved to maven-portal-bom 3.6.3

## [v6.27.0] - 2021-05-10

[#21387] Harmonized version of jackson libs to 2.8.11 to resolve conflict
[#21346] Applying new workflow to manage the "Sync with Tredds" facility
[#21444] Moved to maven-portal-bom >= 3.6.2-SNAPSHOT 

## [v6.26.0] - 2021-04-12

#### Enhancements

[#21153] Upgrade the maven-portal-bom to 3.6.1 version
[#20762] Moved to ckan-metadata-publisher-widget 2.X

## [v6.25.4] - 2021-03-10

#### Enhancements

[#20847] Just to release the facility ws-thredds-sync 

## [v6.25.3] - 2021-02-02

Just to include new SHUB model

## [v6.25.2] - 2021-01-18

#### Bug Fixes

[#20457] Just including patched library

## [v6.25.1] - 2020-10-19

#### Bug Fixes

[#19952] Fixed incident

## [v6.25.0] - 2020-08-25

#### Enhancements

[#19600] revisit the "Get Info" Dialog in a modern view


## [v6.24.1] - 2020-06-22

[#19493] Just to include the ckan-util-library patched


## [v6.24.0] - 2020-05-18

**New Features**

[#19058] Restore operation: the user has to select the destination folder


## [v6.23.2] - 2020-04-21

Just to include the new shub-client released


## [v6.23.1] - 2020-03-27

Ported to git/jenkins


## [v6.23.0] - 2019-09-25

[Task #17226] Removing HL

[Task #17524] Hiding 'Add Administrator" on VRE Folders

[Task #17552] Workspace: reconsider the actions Private Link, Folder Link and Get Link


## [v6.22.0] - 2019-04-05

Updated to StorageHub [ticket: #13226]

{Bug #13328] Bug fixes


## [v6.21.0] - 2018-11-01

Task #13106: Provide public links for versioned files


## [v6.20.0] - 2018-09-01

Task #12604: Migrate Move operation to StorageHub

Incident #12922: Bug fix


## [v6.19.1] - 2018-07-02

Revised Style sheet and updated to common set of icons

[Task #12089] Migrate to workspace-uploader 2.0

[Release #12006] Removed portlets-widgets.wsmail-widget dependency


## [v6.19.0] - 2018-06-07

[Project Activity #11690] Integrated with Task Executor Widget


## [v6.18.0] - 2018-03-01

[Task #11127] Porting to new ws-thredds engine

[Feature #11325] Workspace: add New URL feature in the context menu of right pane


## [v6.17.4] - 2018-01-09

Issue #10831, Workspace download folder tomcat temp occupation issue

[Incident #11113] Bug fixing


## [v6.17.3] - 2017-11-20

Task #9758: bug fixing


## [v6.17.2] - 2017-09-13

Incident #9676: fixed. Removed check on get sub-folder public link when operation is performed by an administrator


## [v6.17.1] - 2017-05-22

Feature #5207: integrate image previewer widget


## [v6.17.0] - 2017-04-12

Task #8070: file versioning info panel added into How-to

Improved responsivness, added collapsible tree panel when device is a phone and shifted download button next to the upload one.


## [v6.16.0] - 2017-03-03

Removed accesslogger dependency

[Task #6988] Fixed Edit administrator showing

[Feature #7006] File Versioning

Edit administrator can be performed also by other Administrator/s

Edit permissions can be performed also by Administrators


## [v6.15.1] - 2017-02-02

Minor fixes


## [v6.15.0] - 2016-11-29

[Feature #5873] Remove ASL Session from the Workspace and its components

Updated the Show event on a object of kind LINK: it is opened


## [v6.14.0] - 2016-09-14

[Feature #2335] Added "Get Folder Link" facility

[Incident #4878] Fixed: Workspace tree not displaying user's name

[Feature #5116] Implemented Show public folders in the Workspace via Smart Folder

[Feature #5110] Added Enabled/Disabled Public Access to workspace history

[Bug #5218] Fixed return to the Workspace root Folder


## [v6.13.0] - 2016-05-31

[Feature #4128] Migration to Liferay 6.2

Data Catalogue publishing supported


## [v6.12.0] - 2016-05-16

Integrated with new workspace uploader

Bug fixed: Css for Dialog Cancel multiple files


## [v6.11.0] - 2016-01-13

[Feature #1925] Added, new public link format: http://host/storageID

[Feature #1298] Update public link generation


## [v6.10.1] - 2015-12-16

Bug Fixing - #1804; #1808; #1822; #1333


## [v6.10.0] - 2015-11-19

[Feature #124] Remove a user from shared folder

[Feature #1259] Enhancement to workspace item history

[Bug #1373] Fixed: breadcrumb slowness on file upload make upload of files on parent folders

[Incident #1338] Fixed: Workspace and ownership ... something is wrong

[Bug #1459] Fixed: get link disabled

[Bug #1260] Fixed: minor glitch on Chrome, topbar shows vertical scroller


## [v6.9.0] - 2015-10-30

[Bug #718] Fixed breadcrumb path

[Bug #546] Fixed bug

[Bug #531] Fixed issue on create folder

[Feature #429] Realized. Integrated with workspace-uploader

[Bug #1270] Fixed. Workspace improvements: many rpc calls after an delete multiple

[Feature #1280] Workspace GUI: quote and available feature improvements


## [v6.8.0] - 2015-07-06

[Feature #129] Porting to HL 2.0

Feature #332] Generate workspace Public Link with SMP-ID


## [v6.7.1] - 2015-04-15

Integrated Contacts Edit Permissions


## [v6.7.0] - 2014-10-29

Added user storage usage

Added notifications for: set folder Administrator, delete shared folder


## [v6.6.7] - 2014-09-01

Fixed support #813: Broken Breadcrumb for long path

Added "Edit Administrators"


## [v6.6.6] - 2014-06-04

Added Trash: https://issue.imarine.research-infrastructures.eu/ticket/2497

Updated pom to support new portal configuration (gcube release 3.2)

Completed activity to https://support.social.isti.cnr.it/ticket/126


## [v6.6.5] - 2014-06-04

Integrated trash: https://issue.imarine.research-infrastructures.eu/ticket/2497

Updated pom to support new portal configuration (gcube release 3.2)


## [v6.6.4] - 2014-05-22

Fixed: https://support.d4science.research-infrastructures.eu/ticket/843

Added: item number in grid and trash. See: https://issue.imarine.research-infrastructures.eu/ticket/282


## [v6.6.2] - 2014-3-17

Management of My Special Folder

Added change permissions to VRE shared folder


## [v6.6.0] - 2014-06-04

Added Trash: https://issue.imarine.research-infrastructures.eu/ticket/2497

Updated pom to support new portal configuration (gcube release 3.2)


## [v6.6.0] - 2014-05-22

Fixed: https://support.d4science.research-infrastructures.eu/ticket/843

Added: item number in grid and trash. See: https://issue.imarine.research-infrastructures.eu/ticket/282


## [v6.6.0] - 2014-03-17

Management of "My Special Folder"

Added change permissions to VRE shared folder


## [v6.6.0] - 2014-02-07

[#2634] Workspace: support for setting permissions over shared folders

[#2633] Workspace: support for VRE Shared Folders

[#2290] Worskspace history operations: should support the session validation


## [v6.5.0] - 2013-12-12

[#Ticket 738] fixed. HomeLibrary initialize Exception


## [v6.5.0] - 2013-10-21

[#Ticket 2223] This project was enhancements to gwt 2.5.1

GCF dependency was removed


## [v6.4.1] - 2013-09-16

Provide support for public link, Related ticket: #1993

Link sharing and public link: generate a human-readable URL via URL shortening, Related ticket: #1921

Fixed bug on shared links, Ticket #630


## [v6.3.0] - 2013-07-08

Provide support for share link, Related ticket: #1504

Edit description on sharing, Related ticket: #1822

Bugs fixed, Related Tickets: #628, #633, #630


## [v6.2.0] - 2013-05-29

Provide support for accounting, related ticket: #1752

Enable notification for file upload in shared folder, related ticket: #1732


## [v6.1.0] - 2013-04-19

Workspace portlet was enhanced to meet the requests coming from the User Community

Related tickets: #1500, #1498, #320, #1487, #1499, #1501, #1497, #1536


## [v6.0.0] - 2013-03-05

#1259 The workspace portlet was mavenized.

Fixed bugs: see #1327, #1278, #1279


## [v5.3.0] - 2013-01-17

Ticket #873: Messaging facility to be part of the Social Portal space

Ticket #872: Workspace Portlet also "outside" of VREs


## [v5.2.0] - 2012-11-30

[#111] New workspace item (resource link) was added in workspace portlet


## [v5.1.0] - 2012-09-21

New Mail Sender Widget added, supporting multiple attachments and multiple recipients

Reply and Reply all features added to messages

Inbox messages sort by date reverse order

Bug fixed: upload file/archive


## [v5.0.0] - 2012-05-04

[#216] New version of Workspace Portlet is developed using the GXT framework

[#216] Reviews workspace GUI and adding new features

[#216] Added toolbar with navigation path of items

[#216] Added toolbar for an easy access to the operations on items ("Add", "Download", "Rename", "Delete", etc.)


## [v4.2.0] - 2011-07-01

[#1555] WorkflowTemplate and WorkflowReport required in Workspace Portlet


## [v4.1.0] - 2011-05-06

GWT 2.2.0 support

added gwt-ext patch


## [v4.0.0] - 2011-02-07

Removed Workspace and Basket type, replaced with folder option

Synch with others changes in HomeLibrary


## [v3.2.0] - 2010-10-22

[#398] Workspace Details panel improvement

used ScopeHelper for session settings

Enabled details panel for Report, ReportTemplate and AquaMapsItem workspace items


## [v3.1.0] - 2010-09-03

[#30] FCPPS / Workspace / Help does not open


## [v3.0.0] - 2010-07-16

Ported to GWT 2.0

Updated project structure to WebPortlet

Update to LifeRay portal


## [v2.6.0] - 2010-05-14

[#424] Workspace code refactoring an enanchement

code refactoring, sync with Workspace Portlet Tree changes

added new details panels for Time Series, AquaMaps Item, Report, Report Template

improved details panels update speed (new panel construction vs static panels)


## [v2.5.0] - 2010-01-29

gcube release 1.7.0


## [v2.4.0] - 2009-11-30

gcube release 1.6.0


## [v2.3.0] - 2009-11-16

gcube release 1.5.0


## [v2.2.0] - 2009-10-16

gcube release


## [v2.1.1] - 2009-07-29

gcube release 1.2.2


## [v2.1.0] - 2009-07-14

gcube release 1.2.0


## [v2.0.0] - 2009-05-19

gcube release 1.2.0 rc1


## [v1.1.2] - 2009-01-12

first release
