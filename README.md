# gCube Workspace

The gCube Workspace is a web-system (made from many components) that built a web-gui to manage the Workspace in the D4Science infrastructure. The Workspace is a collaborative area where users can exchange and organize information objects (workspace items) according to their specific needs.
Every user of any Virtual Research Environment is provided with this area for the exchange of workspace objects (share) with other users. Such an area is further organized in workspaces as to resemble a classic folder-based file system. 

## Built With

* [OpenJDK](https://openjdk.java.net/) - The JDK used
* [Maven](https://maven.apache.org/) - Dependency Management

**Uses**

* GWT v.2.7.0. [GWT](http://www.gwtproject.org) is licensed under [Apache License 2.0](http://www.gwtproject.org/terms.html)
* GWT-Bootstrap v.2.3.2.0. [GWT-Bootstrap](https://github.com/gwtbootstrap) is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0)
* GXT v2.2.5 is licensed under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)

**Related Components**

* [Workpsace Tree Widget](https://code-repo.d4science.org/gCubeSystem/workspace-tree-widget)
* [Workpsace Sharing Widget](https://code-repo.d4science.org/gCubeSystem/workspace-sharing-widget)
* [Task Executor Widget](https://code-repo.d4science.org/gCubeSystem/ws-task-executor-widget)
* [Catalogue Metadata Publisher Widget](https://code-repo.d4science.org/gCubeSystem/ckan-metadata-publisher-widget)
* [StorageHub Client Wrapper](https://code-repo.d4science.org/gCubeSystem/storagehub-client-wrapper)

and others. You can discovery all dependencies via dependency hierarchy (e.g. use [dependency:tree](https://maven.apache.org/plugins/maven-dependency-plugin/tree-mojo.html))

## Showcase

**Workspace Home**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/c/cf/Showcase_Workspace.png" style="max-width:800px;" alt="Workspace Home" />

**Workspace 'Context Menu' facility**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/2/2d/Workspace_Context_Menu.png" style="max-width:800px;" alt="Workspace Context Menu" />

**Workspace 'Get Info' facility**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/2/27/Showcase_Workspace_Get_Info.png" style="max-width:800px;" alt="Workspace Get Info" />
<br />
<img src="https://gcube.wiki.gcube-system.org/images_gcube/8/83/Showcase_Workspace_GetInfo3.png" style="max-width:800px;" alt="Workspace Get Info 3" />

**Workspace 'Get Shareable Link' facility**

<img src="https://gcube.wiki.gcube-system.org/images_gcube/7/72/Showcase_Workspace_GetShareableLink.png" style="max-width:800px;" alt="Workspace Get Shareable Link" />

## Documentation

You can find the Workspace documentation at [Wiki gCube Workpsace](https://wiki.gcube-system.org/workspace)

## Change log

See the [Releases](https://code-repo.d4science.org/gCubeSystem/workspace/releases)

## Authors

* **Francesco Mangiacrapa** ([ORCID](https://orcid.org/0000-0002-6528-664X)) Computer Scientist at [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260).
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488);
    - D4Science-II (grant no.239019);
    - ENVRI (grant no. 283465);
    - EUBrazilOpenBio (grant no. 288754);
    - iMarine(grant no. 283644).
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680);
    - EGIEngage (grant no. 654142);
    - ENVRIplus (grant no. 654182);
    - PARTHENOS (grant no. 654119);
    - SoBigData (grant no. 654024);
    - DESIRA (grant no. 818194);
    - ARIADNEplus (grant no. 823914);
    - RISIS2 (grant no. 824091);
    - PerformFish (grant no. 727610);
    - AGINFRAplus (grant no. 731001).


