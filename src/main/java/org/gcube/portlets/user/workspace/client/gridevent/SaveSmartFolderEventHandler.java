package org.gcube.portlets.user.workspace.client.gridevent;

import com.google.gwt.event.shared.EventHandler;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa{@literal @}isti.cnr.it
 *
 */
public interface SaveSmartFolderEventHandler extends EventHandler {
	void onSaveSmartFolder(SaveSmartFolderEvent saveSmartFolderEvent);
}